import java.net.*;
import java.io.*;

public class client extends Thread {

	private static DatagramSocket client;
	private static InetAddress ip;
	private static int porta;

	public client(DatagramSocket client) {
		this.client = client;
	}

	public static void main(String[] args) throws IOException{
		//"192.168.2.8" - ip do computador servidor
		//4999 - porta utilizada
		DatagramSocket s = new DatagramSocket();
		String servidor = "192.168.2.8";
		porta = 4999;

		ip = InetAddress.getByName(servidor);
		
		client c = new client(s);
		Thread t = new Thread(c);
		t.start();
	}

	//sobreescrita do metodo run(), executado pela thread
	@Override
	public void run() {
		try {
			byte[] sendData = new byte[1024];
			byte[] receiveData = new byte[1024];

			//envia a string para o server
			String enviar = "a b d e f c y t u s";
			sendData = enviar.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ip, porta);
			client.send(sendPacket);

			//recebe o resultado do server
			DatagramPacket receivePacket = new DatagramPacket(receiveData,
				receiveData.length);
			client.receive(receivePacket);

			String modifiedSentence = new String(receivePacket.getData());
			//System.out.println("Tempo ida e volta mensagem: " + (System.currentTimeMillis() - tempoInicio) + "ms");

			//imprime a string ordenada pelo server
			System.out.println("server: " + modifiedSentence);
			//client.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}