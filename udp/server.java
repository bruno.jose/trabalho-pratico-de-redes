import java.net.*;
import java.io.*;

public class server extends Thread {
	private static int porta = 4999;
	private static int numConn = 1;
	
	private static DatagramSocket serverSocket;
	private static DatagramPacket receivePacket;

	private static byte[] receiveData = new byte[1024];
	private static byte[] sendData = new byte[1024];

	public server(DatagramPacket r){
		this.receivePacket = r;
	}

	public static void main(String[] args) throws IOException {
		serverSocket = new DatagramSocket(porta);

		while(true) {
			try {
				DatagramPacket r = new DatagramPacket(receiveData,
						receiveData.length);
				System.out.println("Esperando por datagrama UDP na porta " + porta);
				serverSocket.receive(r);
				System.out.print("Datagrama UDP [" + numConn + "] recebido...");

				server s = new server(r);
				Thread t = new Thread(s);
				t.start();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}

	//sobreescrita do metodo run(), executado pela thread
	@Override
	public void run() {
		try {

			String sentence = new String(receiveData, "UTF-8");
			System.out.println(sentence);
			
			InetAddress IPAddress = receivePacket.getAddress();

			int port = receivePacket.getPort();

			//String capitalizedSentence = sentence.toUpperCase();
			//transforma a string em um array de strings para ordena-lo
			String[] arrayString = sentence.split(" ");
			
			//aplica o algoritmo de ordenação
			arrayString = insertionSort(arrayString);

			//converte array em uma unica string
			String str = convertArrayToStringMethod(arrayString);

			sendData = str.getBytes();

			DatagramPacket sendPacket = new DatagramPacket(sendData,
					sendData.length, IPAddress, port);

			System.out.print(str);

			serverSocket.send(sendPacket);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	//insertion sort (algoritmo que ordena o array)
	public static String[] insertionSort(String a []) {
        if (a == null) 
        	return a;   
        
        int i,j;  
        String x;
        for (j = 1; j < a.length; j++) {       
            x = a[j]; 
            i = j - 1;

            while (i >= 0) {
                if (x.compareTo(a[i]) > 0) {
                    break;
                }
                a[i + 1] = a[i];
                i--;
            }
            a[i + 1] = x;
        }
                
        return a;
	}

	//transforma o array em uma string, para enviar ao client
	public static String convertArrayToStringMethod(String[] strArray) {
        StringBuilder stringBuilder = new StringBuilder();
        
        for (int i = 0; i < strArray.length; i++) {
            stringBuilder.append(strArray[i]);
        }

        return stringBuilder.toString();
    }
}