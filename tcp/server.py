import socket
import time
import settings
from random import randint

def generateRandomNumber(collaborators, collaborator):
    randomNumber = randint(0, len(collaborators) - 1)

    while randomNumber == collaborator:
        randomNumber = randint(0, len(collaborators) - 1)

    return randomNumber

def addClientsWasCollaborator (collaborators, serverSocket):
    # Servidor iniciado em modo passivo
    print('\nSERVER STARTED IN PASSIVE MODE\n')

    for client in range(settings.MAX_CLIENTS):     #Esperando o número máx de clientes se conectar
        newClient, address = serverSocket.accept()  #Aceitando a conexão de um novo cliente
        clientMessage = newClient.recv(2048).decode('utf-8') #Recebendo a confirmação de cliente ativo e o número de solicitações
        solicitationNumber = int(clientMessage[slice(len(clientMessage) - 1, len(clientMessage), 1)]) #Obtendo o somente o número de solicitações
        collaborators.append({'id': client, 'client': newClient, 'solicitationNumber': solicitationNumber}) #Adicionando o cliente em collaborators (dict)
        print ('CLIENT ' + str(client) + ' connected | IP - ' + address[0] + ':' + str(address[1]) + ' | solicitations : ' + str(solicitationNumber))


def runSolicitations(collaborators):
    print('\n')
    for collaborator in range(len(collaborators)):
        for i in range(collaborators[collaborator]['solicitationNumber']):

            #pedindo para o client mandar uma solicitação
            collaborators[collaborator]['client'].send(str.encode("send solicitation"))
            solicitation = collaborators[collaborator]['client'].recv(2048).decode('utf-8')
            print ("SOLICITATION RECEIVED FROM CLIENT " + str(collaborator) + ' | STRING -> ' + solicitation)

            #enviando a solicitação para outro cliente aleatório
            randomNum = generateRandomNumber(collaborators, collaborator)
            collaborators[randomNum]['client'].send(str.encode(solicitation))
            print("STRING SENT TO CLIENT " + str(randomNum))

            #recebendo a resposta do processamento da string
            response = collaborators[randomNum]['client'].recv(2048).decode('utf-8')
            print("STRING RESPONSE RECEIVE FROM CLIENT " + str(randomNum))

            #mandando a resposta para o cliente que solicitou o processamento
            collaborators[collaborator]['client'].sendall(str.encode('RESPONSE: ' + response))
            print("SOLICITATION RESPONSE SENT BACK TO CLIENT " + str(collaborator) + ' | RESPONSE -> ' + response + '\n')
            time.sleep(0.5)

    for collaborator in range(len(collaborators)):
        collaborators[collaborator]['client'].send(str.encode("end of sections"))

def main():

    serverSocket = socket.socket()
    host = '127.0.0.1'
    port = 1233
    collaborators = []

    try:
        serverSocket.bind((host, port))
    except socket.error as e:
        print(str(e))

    serverSocket.listen(5)

    addClientsWasCollaborator(collaborators, serverSocket)
    print(collaborators)
    runSolicitations(collaborators)

    serverSocket.close()

main()