import socket
import sys

def stringProcess(msg):
    cons = 0
    vog = 0
    num = 0

    if len(msg) == 0:
        return 'erro'

    for i in msg:
        if i == 'a' or i == 'e' or i == 'i' or i == 'o' or i == 'u':
            vog += 1

        elif i == 'b' or i == 'c' or i == 'd' or i == 'f' or i == 'g' \
                or i == 'j' or i == 'k' or i == 'l' or i == 'm' or i == 'n' \
                or i == 'p' or i == 'q' or i == 'r' or i == 's' or i == 't' \
                or i == 'v' or i == 'w' or i == 'x' or i == 'z':
            cons += 1

        elif i == '0' or i == '1' or i == '2' or i == '3' or i == '4' or i == '5' or i == '6' \
                or i == '7' or i == '8' or i == '9':
            num += 1

    if cons == 0 and vog == 0 and num == 0:
        return 'erro'
    else:
       return f"C={str(cons)};V={str(vog)};N={str(num)}"

def run(ClientSocket, txtInfos):

    solicitation = 1

    while True:

        Response = ClientSocket.recv(1024)

        # quando o servidor permitir o envio de solicitações é enviada uma por uma
        if Response.decode('utf-8') == "send solicitation":
            ClientSocket.send(str.encode(F"string: {txtInfos[solicitation]}"))
            print('============= SEND SOLICITATION TO SERVER =================')
            print('SERVER RESPONSE = ' + Response.decode('utf-8'))
            print('SENT TO SERVER = ' + F"string: {txtInfos[solicitation]}\n")
            solicitation += 1

        # caso o servidor envie uma string para ser processada
        elif Response.decode('utf-8')[slice(7)] == "string:":
            serverMsg = Response.decode('utf-8')
            serverMsgSlice = serverMsg[slice(8, len(serverMsg))]
            response = stringProcess(serverMsgSlice)
            ClientSocket.send(str.encode(response))
            print('========= STRING PROCESSING FOR OTHER CLIENTS =============')
            print('SERVER RESPONSE = ' +  Response.decode('utf-8'))
            print('SENT TO SERVER  = ' +  response + "\n")

        # caso o servidor envie uma resposta de solicitação desse cliente
        elif Response.decode('utf-8')[slice(9)] == "RESPONSE:":
            print('========= RESPONSE PROCESS FROM OTHER CLIENT ==============')
            print(Response.decode('utf-8') + '\n')

            with open(F"{sys.argv[2]}", 'a') as arq:
                    arq.write(Response.decode('utf-8')[slice(10, len(Response.decode('utf-8')))] + '\n')

        # Caso o servidor informe o fim dos trabalhos
        elif Response.decode('utf-8') == "end of sections":
            print('===================== END OF SECTIONS =====================')
            print('SERVER RESPONSE = ' + Response.decode('utf-8'))
            break


def readArq():
    textInputs = ""

    with open(F"{sys.argv[1]}", 'r') as arq:
        for i in arq:
            li = i.strip()
            if not li.startswith('//'):
                textInputs += i.rstrip() + '\n'

    textInfos = textInputs.split('\n')
    listInfos = [word.strip() for word in textInfos if word.strip() != ""]
    return listInfos

def main():

    ClientSocket = socket.socket()
    host = '127.0.0.1'
    port = 1233

    try:
        ClientSocket.connect((host, port))
    except socket.error as e:
        print(str(e))

    txtInfos = readArq()

    ClientSocket.send(str.encode(F"status: client activated, solicitationNumber: {txtInfos[0]}"))
    print('SEND TO SERVER = ' + 'status: client activated, solicitationNumber: 5\n')

    run(ClientSocket, txtInfos)

    ClientSocket.close()

main()