import socket
import time
from _thread import *

ServerSocket = socket.socket()
host = '127.0.0.1'
port = 1233
collaborators = []
numbersCollaborators = 0

try:
    ServerSocket.bind((host, port))
except socket.error as e:
    print(str(e))

#Servidor iniciado em modo passivo
print('\nSERVER STARTED IN PASSIVE MODE\n')
ServerSocket.listen(5)

def threaded_client(client):
    start = time.time()
    while True:

        #Recebendo mensagem do client
        data = client.recv(2048)

        #caso a mensagem seja uma notificação de conecção do cliente
        if data.decode('utf-8')[slice(7)] == "status:":
            client.send(str.encode("You are a collaborator now"))
            print('CLIENT SEND            -> ' + data.decode('utf-8'))
            print('SEND TO CLIENT         -> ' + 'You are a collaborator now')

        # caso a mensagem seja uma requisição com uma string
        elif data.decode('utf-8')[slice(20)] == "solicitation string:":
            if len(collaborators) > 1:
                collaborators[0].send(str.encode(data.decode('utf-8')))
                print('CLIENT SEND            -> ' + data.decode('utf-8'))
                print('SEND TO ANOTHER CLIENT -> ' + data.decode('utf-8'))
            else:
                print('JUST ONE COLLABORATOR')

        # caso a mensagem seja o resultado processado por um cliente
        #elif response with correct result
        #print(V=2,C=8,N=2)
        #send back to client

        if start < start + 10000:
            for client in collaborators:
                print('SEND TO CLIENT -> ' + 'end of sections')
                client.send(str.encode("end of sections"))
            break

        if not data:
            break

    client.close()

while True:
    Client, address = ServerSocket.accept()
    collaborators.append(Client)
    #start_new_thread(threaded_client, (Client,))
    numbersCollaborators += 1

    print('\nClient: ' + address[0] + ':' + str(address[1]))

ServerSocket.close()


#name clients by id and create a new class to make this